// Project: blconvert
// Description: converts a blacklist in hosts-file format to dnsmasq format
// Author: Christopher Cover https://pobox.com/~cxc/
// Copyright: Copyright (c) 2018 Christopher Cover.
// License: MIT
// Date: Dec 18, 2018
// Version: 0.0.1
//
// The MIT License (MIT)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

import Foundation
import SystemConfiguration

let numOpts = CommandLine.arguments.count // same value as argc
let options = CommandLine.arguments // the string of options and arguments
let program = CommandLine.arguments.first! // the script or program name, unwrapped
let version = "0.0.1"

func showVersion() {
    print("This is blconvert version \(version).")
}

func notify(message: String) {
    if (isatty(STDOUT_FILENO) == 1) {
        print("\(message)")
    } else {
        NSLog(message)
    }
}

func showUsage() {
    print("Convert a hosts file to dnsmasq format.")
    print("Usage: \(program)")
    print("  [-f,--file <arg>]")
    print("  [-h,--help]")
    print("  [-v,--version]")
    print("")
    print("Example: \(program) -f \"$HOME/etc/hosts\"")
    print("Example: \(program) -f \"$HOME/etc/hosts\" > /usr/local/etc/dnsmasq.d/banned.conf")
}

func formatHostsToDnsmasq(src: String) -> String? {
    guard let reader = LineReader(path: src) else {
        return nil;
    }
    
    var trimmedLine = ""
    var hostDefinitionParts = [String]()
    var newFileText = ""

    for line in reader {
        // Not interested in commented lines.
        if (line.hasPrefix("0.0.0.0")) {
            // Remove whitespace and newlines from beginning and end of line.
            trimmedLine = line.trimmingCharacters(in: .whitespacesAndNewlines)
            // Split the line into its IP address and hostname parts.
            hostDefinitionParts = trimmedLine.components(separatedBy: " ")
            // Not interested in malformed lines.
            if (hostDefinitionParts[1] != "0.0.0.0") {
                // Stack each dnsmasq formatted definition into one string with a newline per line.
                newFileText += "address=/" + hostDefinitionParts[1] + "/0.0.0.0\n"
            }
        }
    }

    return newFileText
}

switch numOpts {
// We get 1 option when the program is run with no options.
case 1:
    showUsage()
// We get 2 options when it runs with '-v', '-h', and error.
case 2:
    let option = CommandLine.arguments[1]
    switch option {
    case "-h", "--help":
        showUsage()
    case "-v", "--version":
        showVersion()
    default:
        notify(message: "Unknown option '\(option)' or missing option argument.")
        showUsage()
    }
// We get 3 options when it runs with '-f arg'.
case 3:
    let option = CommandLine.arguments[1]
    let optArg = CommandLine.arguments[2]
    switch option {
    case "-f", "--file":
        let fileManager = FileManager.default
        if (fileManager.fileExists(atPath: optArg)) {
            print(formatHostsToDnsmasq(src: optArg)!)
        } else {
            notify(message: "File does not exist: '\(optArg)'.")
        }
    case "-h", "--help":
        showUsage()
    case "-v", "--version":
        showVersion()
    default:
        notify(message: "Unknown option '\(option)' or missing option argument.")
        showUsage()
    }
// With any more than three options, the program provides a usage note.
default:
    showUsage()
}
